<?php

class User extends Controller{  
  public function index() {
    $data['title'] = 'User';

    $this->view('Templates/header', $data);
    $this->view('User/index');
    $this->view('Templates/footer');
}

public function profile($nama = "Homelander", $pekerjaan = "Superhero") {
    $data["nama"] = $nama;
    $data["pekerjaan"] = $pekerjaan;

    $this->view('Templates/header', $data);
    $this->view('User/profile', $data);
    $this->view('Templates/footer');
  }
}

?>