<div class="container mt-5">
   <div class="row">
       <div class="col-6">
           <h3>Blog</h3>
           <!-- Button trigger modal -->
           <button type="button" class="btn btn-info mt-2 mb-4" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Data
            </button>
               <?php foreach($data["blog"] as $blog) :?>
               <ul>
                   <li class="list-group-item d-flex justify-content-between align-item-center">
                    <?= $blog['judul'] ?>
                    <div class="aksi">
                    <a href="<?= BASE_URL ?>/blog/detail/<?= $blog['id']; ?>" class="btn btn-info">READ</a>
                    <a href="" class="btn btn-warning">EDIT</a>
                    <a href="" class="btn btn-danger">DELETE</a>
                    </div>
                   </li>
               </ul>
           <?php endforeach; ?>
       </div>
   </div>
</div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="<?= BASE_URL ?>/blog/tambah">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username">
                    </div>
                    <div class="mb-3">
                        <label for="judul" class="form-label">Judul Tulisan</label>
                        <input type="text" class="form-control" id="judul" placeholder="Judul Tulisan" name="judul">
                    </div>
                    <div class="mb-3">
                        <label for="Isi tulisan" class="form-label">Isi tulisan</label>
                        <textarea class="form-control" id="tulisan" rows="3" name="tulisan"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info" name="submit">Save changes</button>
                </div>
                </div>
                </form>
            </div>
            </div>